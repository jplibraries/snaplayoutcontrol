﻿using System;

namespace SnapLayoutWpfApp.Commands
{
    public sealed class RelayCommand : CommandBase, IRelayCommand
    {
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;

        public RelayCommand(Action<object> execute) : this(execute, null) { }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
            this.canExecute = canExecute;
        }

        public override bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter) && base.CanExecute(parameter);
        }

        public override void Execute(object parameter)
        {
            execute(parameter);
        }

        public void NotifyCanExecuteChanged()
        {
            OnCanExecuteChanged();
        }
    }

    public class RelayCommand<T> : IRelayCommand<T>
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute) : this(execute, null) { }

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute((T)parameter);
        }

        public bool CanExecute(T parameter)
        {
            return _canExecute == null || _canExecute((T)parameter);
        }

        public void Execute(T parameter)
        {
            _execute((T)parameter);
        }

        public void Execute(object parameter)
        {
            _execute((T)parameter);
        }

        public void NotifyCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
