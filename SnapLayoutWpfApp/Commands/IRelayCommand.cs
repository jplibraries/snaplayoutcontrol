﻿using System.Windows.Input;

namespace SnapLayoutWpfApp.Commands
{
    public interface IRelayCommand : ICommand
    {
        /// <summary>
        /// Notifies that the <see cref="CanExecute(Object)"/> property has changed.
        /// </summary>
        void NotifyCanExecuteChanged();
    }

    public interface IRelayCommand<in T> : IRelayCommand, ICommand
    {
        bool CanExecute(T parameter);
        void Execute(T parameter);
    }
}