﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SnapLayoutWpfApp.Converters
{
    public class WindowsStateToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is WindowState state)
            {
                if (state == WindowState.Normal)
                {
                    return false;
                }

                if (state == WindowState.Maximized)
                {
                    return true;
                }
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool isMaximized)
            {
                return !isMaximized ? WindowState.Normal : WindowState.Maximized;//Change status
            }

            return WindowState.Normal;
        }
    }
}
