﻿using SnapLayoutWpfApp.Commands;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Helpers;

namespace SnapLayoutWpfApp
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public RelayCommand RefreshCommand { get; set; }
        public RelayCommand MinimizeCommand { get; set; }
        public RelayCommand MaximizeRestoreCommand { get; set; }
        public RelayCommand CloseCommand { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            CloseCommand = new RelayCommand(action => this.Close());
            MinimizeCommand = new RelayCommand(action => this.WindowState = WindowState.Minimized);
            MaximizeRestoreCommand = new RelayCommand(action => this.WindowState ^= WindowState.Maximized);

            //WindowState = WindowState.Maximized;

            _ = new SnapLayoutMenu(this, (ButtonBase)this.FindName("btnMaximizeRestore"));
            _ = new WindowResizer(this);
        }
    }
}
