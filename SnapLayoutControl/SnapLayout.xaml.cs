﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Screen;
using System.Windows.Helpers;
using System.Windows.Threading;

namespace SnapLayoutControl
{
    /// <summary>
    /// Logika interakcji dla klasy SnapLayout.xaml
    /// </summary>
    public partial class SnapLayout : UserControl
    {
        private readonly DispatcherTimer dispatcherTimerSnapLayoutHide = new DispatcherTimer();
        private readonly DispatcherTimer dispatcherTimerSnapLayoutShow = new DispatcherTimer();
        private readonly bool isWindows11 = OSVersionHelper.IsWindows11_OrGreater;

        public SnapLayout()
        {
            InitializeComponent();

            dispatcherTimerSnapLayoutHide.Interval = TimeSpan.FromMilliseconds(200);
            dispatcherTimerSnapLayoutHide.Tick += (sender, e) =>
            {
                dispatcherTimerSnapLayoutHide.Stop();
                popup.IsOpen = false;
            };

            dispatcherTimerSnapLayoutShow.Interval = TimeSpan.FromMilliseconds(400);
            dispatcherTimerSnapLayoutShow.Tick += (sender, e) =>
            {
                IsWideScreen = SystemParameters.PrimaryScreenWidth > WideScreenWidth;
                popup.IsOpen = true;
            };

            contentBox.MouseEnter += OnMouseEnter;
            contentBox.MouseLeave += OnMouseLeave;
        }

        #region Hover Base DependencyProperty
        public new Brush Background
        {
            get { return (Brush)GetValue(BackgroundProperty); }
            set { SetValue(BackgroundProperty, value); }
        }

        public static new readonly DependencyProperty BackgroundProperty =
            DependencyProperty.Register("Background", typeof(Brush), typeof(SnapLayout), new PropertyMetadata(new SolidColorBrush(Colors.WhiteSmoke)));

        public new Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        public static new readonly DependencyProperty BorderBrushProperty =
            DependencyProperty.Register("BorderBrush", typeof(Brush), typeof(SnapLayout), new PropertyMetadata(new SolidColorBrush(Colors.Silver)));

        public new Thickness BorderThickness
        {
            get { return (Thickness)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }

        public static new readonly DependencyProperty BorderThicknessProperty =
            DependencyProperty.Register("BorderThickness", typeof(Thickness), typeof(SnapLayout), new PropertyMetadata(new Thickness(1)));

        #endregion

        #region Events
        public event EventHandler SnapClicked;
        public event EventHandler Snaped;

        protected virtual void OnSnapClicked()
        {
            popup.IsOpen = false;
            SnapClicked?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnSnaped()
        {
            Snaped?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        public Window WindowHandle
        {
            get { return (Window)GetValue(WindowHandleProperty); }
            set { SetValue(WindowHandleProperty, value); }
        }

        public static readonly DependencyProperty WindowHandleProperty =
            DependencyProperty.Register("WindowHandle", typeof(Window), typeof(SnapLayout), new PropertyMetadata(null));

        public ButtonBase PlacementTarget
        {
            get { return (ButtonBase)GetValue(PlacementTargetProperty); }
            set { SetValue(PlacementTargetProperty, value); }
        }

        public static readonly DependencyProperty PlacementTargetProperty =
            DependencyProperty.Register("PlacementTarget", typeof(ButtonBase), typeof(SnapLayout), new PropertyMetadata(null, OnPlacementTargeted));

        private static void OnPlacementTargeted(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SnapLayout snapLayout)
            {
                snapLayout.PlacementTarget.MouseEnter += snapLayout.OnMouseEnter;
                snapLayout.PlacementTarget.MouseLeave += snapLayout.OnMouseLeave;
                snapLayout.PlacementTarget.Click += (sender, arg) =>
                {
                    snapLayout.popup.IsOpen = false;
                };
            }
        }

        public int WideScreenWidth
        {
            get { return (int)GetValue(WideScreenWidthProperty); }
            set { SetValue(WideScreenWidthProperty, value); }
        }

        public static readonly DependencyProperty WideScreenWidthProperty =
            DependencyProperty.Register("WideScreenWidth", typeof(int), typeof(SnapLayout), new PropertyMetadata(1280));

        [Category("Appearance")]
        public Brush CardBackground
        {
            get { return (Brush)GetValue(CardBackgroundProperty); }
            set { SetValue(CardBackgroundProperty, value); }
        }

        public static readonly DependencyProperty CardBackgroundProperty =
            DependencyProperty.Register("CardBackground", typeof(Brush), typeof(SnapLayout), new PropertyMetadata(new SolidColorBrush(Colors.Gainsboro)));

        [Category("Appearance")]
        public Brush CardBorderBrush
        {
            get { return (Brush)GetValue(CardBorderBrushProperty); }
            set { SetValue(CardBorderBrushProperty, value); }
        }

        public static readonly DependencyProperty CardBorderBrushProperty =
            DependencyProperty.Register("CardBorderBrush", typeof(Brush), typeof(SnapLayout), new PropertyMetadata(new SolidColorBrush(Colors.DarkGray)));

        [Category("Appearance")]
        public Brush CardMouseOverBackground
        {
            get { return (Brush)GetValue(CardMouseOverBackgroundProperty); }
            set { SetValue(CardMouseOverBackgroundProperty, value); }
        }

        public static readonly DependencyProperty CardMouseOverBackgroundProperty =
            DependencyProperty.Register("CardMouseOverBackground", typeof(Brush), typeof(SnapLayout), new PropertyMetadata(ThemeColor.AccentBrush.Brightness(10).ChangeOpacity(0.6)));

        [Category("Appearance")]
        public Thickness CardBorderThickness
        {
            get { return (Thickness)GetValue(CardBorderThicknessProperty); }
            set { SetValue(CardBorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty CardBorderThicknessProperty =
            DependencyProperty.Register("CardBorderThickness", typeof(Thickness), typeof(SnapLayout), new PropertyMetadata(new Thickness(1)));


        [Category("Behavior")]
        public double DelayOpenMilliseconds
        {
            get { return (double)GetValue(DelayOpenMillisecondsProperty); }
            set { SetValue(DelayOpenMillisecondsProperty, value); }
        }

        public static readonly DependencyProperty DelayOpenMillisecondsProperty =
            DependencyProperty.Register("DelayOpenMilliseconds", typeof(double), typeof(SnapLayout), new PropertyMetadata(400d, OnDelayOpenChanged));

        private static void OnDelayOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SnapLayout snapLayout)
            {
                snapLayout.dispatcherTimerSnapLayoutShow.Interval = TimeSpan.FromMilliseconds(snapLayout.DelayOpenMilliseconds);
            }
        }

        [Category("Behavior")]
        public double DelayCloseMiliseconds
        {
            get { return (double)GetValue(DelayCloseMilisecondsProperty); }
            set { SetValue(DelayCloseMilisecondsProperty, value); }
        }

        public static readonly DependencyProperty DelayCloseMilisecondsProperty =
            DependencyProperty.Register("DelayCloseMiliseconds", typeof(double), typeof(SnapLayout), new PropertyMetadata(200d, OnDelayCloseChanged));

        private static void OnDelayCloseChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SnapLayout snapLayout)
            {
                snapLayout.dispatcherTimerSnapLayoutHide.Interval = TimeSpan.FromMilliseconds(snapLayout.DelayCloseMiliseconds);
            }
        }

        public bool IsEnabledOnWindows11
        {
            get { return (bool)GetValue(IsEnabledOnWindows11Property); }
            set { SetValue(IsEnabledOnWindows11Property, value); }
        }

        public static readonly DependencyProperty IsEnabledOnWindows11Property =
            DependencyProperty.Register("IsEnabledOnWindows11", typeof(bool), typeof(SnapLayout), new PropertyMetadata(false));


        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            dispatcherTimerSnapLayoutShow.Stop();
            dispatcherTimerSnapLayoutHide.Start();
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            if (isWindows11)
            {
                if (IsEnabledOnWindows11)
                {
                    dispatcherTimerSnapLayoutShow.Start();
                    dispatcherTimerSnapLayoutHide.Stop();
                }
            }
            else
            {
                dispatcherTimerSnapLayoutShow.Start();
                dispatcherTimerSnapLayoutHide.Stop();
            }
        }

        #region Private DependencyProperty
        //public bool IsOpen
        //{
        //    get { return (bool)GetValue(IsOpenProperty); }
        //    private set { SetValue(IsOpenPropertyKey, value); }
        //}

        //private static readonly DependencyPropertyKey IsOpenPropertyKey =
        //    DependencyProperty.RegisterReadOnly("IsOpen", typeof(bool), typeof(SnapLayout), new PropertyMetadata(false));

        //public static readonly DependencyProperty IsOpenProperty = IsOpenPropertyKey.DependencyProperty;

        public bool IsWideScreen
        {
            get { return (bool)GetValue(IsWideScreenProperty); }
            private set { SetValue(IsWideScreenPropertyKey, value); }
        }

        public static readonly DependencyPropertyKey IsWideScreenPropertyKey =
            DependencyProperty.RegisterReadOnly("IsWideScreen", typeof(bool), typeof(SnapLayout), new PropertyMetadata(true));

        public static readonly DependencyProperty IsWideScreenProperty = IsWideScreenPropertyKey.DependencyProperty;
        #endregion

        #region SnapAlgoritm
        private void MoveToScreenPart(uint varticalParts, uint varticalPartIndex, uint varticalSpan, uint horizontalParts, uint horizontalPartIndex, uint horizontalSpan)
        {
            if (varticalParts == 0 || horizontalParts == 0)
            {
                Debug.WriteLine("SnapLayout: Ilość części nie może mieć wartość 0.");
                return;
            }

            if (varticalPartIndex + 1 > varticalParts || horizontalPartIndex + 1 > horizontalParts)
            {
                Debug.WriteLine("SnapLayout: Index części nie może być wiekszy od ilości cześci.");
                return;
            }

            OnSnapClicked();

            if (WindowHandle is null)
            {
                WindowHandle = Application.Current.MainWindow;
            }

            ScreenSize screen = WindowHandle.CurrentScreenSize();

            Size partSize = new Size
            {
                Width = screen.WorkingArea.Width / varticalParts,
                Height = screen.WorkingArea.Height / horizontalParts,
            };

            if (WindowHandle.WindowState == WindowState.Maximized)
            {
                WindowHandle.WindowState = WindowState.Normal;
            }

            Point newPosition = new Point
            {
                // Left
                X = screen.WorkingArea.Width == partSize.Width ? 0 : (varticalPartIndex * partSize.Width) + screen.WorkingArea.Left,
                // Top
                Y = screen.WorkingArea.Height == partSize.Height ? 0 : (horizontalPartIndex * partSize.Height) + screen.WorkingArea.Top
            };
            Size newSize = new Size
            {
                Width = partSize.Width * varticalSpan < WindowHandle.MinWidth ? WindowHandle.MinWidth : partSize.Width * varticalSpan,
                Height = partSize.Height * horizontalSpan < WindowHandle.MinHeight ? WindowHandle.MinHeight : partSize.Height * horizontalSpan
            };

            // Correct position when app goes off screen.
            if (newPosition.X + newSize.Width > screen.WorkingArea.Width)
            {
                double expandBeyond = newPosition.X + newSize.Width - screen.WorkingArea.Left;
                newPosition.X -= (expandBeyond - screen.WorkingArea.Width);
            }
            if (newPosition.Y + newSize.Height > screen.WorkingArea.Height)
            {
                double expandBeyond = newPosition.Y + newSize.Height - screen.WorkingArea.Top;
                newPosition.Y -= (expandBeyond - screen.WorkingArea.Height);
            }

            WindowHandle.Top = newPosition.Y;
            WindowHandle.Left = newPosition.X;
            WindowHandle.Width = newSize.Width;
            WindowHandle.Height = newSize.Height;

            OnSnaped();
        }

        //public enum SnapPartType
        //{
        //    LeftHalf,
        //    //MiddleHalf,
        //    RightHalf,

        //    ThirdLeftHalf,
        //    ThirdMiddleHalf,
        //    ThirdRightHalf,

        //    LeftGreaterHalf,
        //    //RightGreaterHalf,
        //    //LeftLessHalf,
        //    RightLessHalf,

        //    LeftUpperHalf,
        //    RightUpperHalf,
        //    LeftLowerHalf,
        //    RightLowerHalf,

        //    LeftSlimHalf,
        //    RightSlimHalf,
        //    MiddleFatHalf,
        //}

        private void SnapLeftHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 0, 1, 1, 0, 1);
        }

        private void SnapRightHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 1, 1, 1, 0, 1);
        }

        private void SnapLeftGreaterHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(3, 0, 2, 1, 0, 1);
        }

        private void SnapRightSmallerHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(3, 2, 1, 1, 0, 1);
        }

        private void SnapRightBottomHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 1, 1, 2, 1, 1);
        }

        private void SnapRightUpperHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 1, 1, 2, 0, 1);
        }

        private void SnapLeftUpperHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 0, 1, 2, 0, 1);
        }

        private void SnapLeftBottomHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(2, 0, 1, 2, 1, 1);
        }

        private void SnapLeftSlimHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(5, 0, 1, 1, 0, 1);
        }

        private void SnapRightSlimHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(5, 4, 1, 1, 0, 1);
        }

        private void SnapMiddleHalf(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(5, 1, 3, 1, 0, 1);
        }

        private void SnapThirdLeftPart(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(3, 0, 1, 1, 0, 1);
        }

        private void SnapThirdMiddlePart(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(3, 1, 1, 1, 0, 1);
        }

        private void SnapThirdRightPart(object sender, MouseButtonEventArgs e)
        {
            MoveToScreenPart(3, 2, 1, 1, 0, 1);
        }
        #endregion
    }
}
